require "wisper"

class Choose
 include Wisper::Publisher

 SEBA = "seba".freeze
 TOMEK = "tomek".freeze

  def call
    broadcast("its_#{[TOMEK, SEBA].sample}")
  end
end
